import FWCore.ParameterSet.Config as cms
from EgammaUser.EgammaPostRecoTools.EgammaPostRecoTools import setupEgammaPostRecoSeq

#cf. https://twiki.cern.ch/twiki/bin/view/CMS/EgammaPostRecoRecipes

def customizeEGM(process,era,runWithAOD=False,preVFP=False):

    if '2016' in era: 
        egmEra='2016preVFP-UL'
        if not preVFP: egmEra='2016postVFP-UL'

    setupEgammaPostRecoSeq(process,
                           isMiniAOD=True,
                           era=egmEra,
                           runEnergyCorrections=False)

    process.egammaPostReco=cms.Path(process.egammaPostRecoSeq)
