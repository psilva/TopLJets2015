import FWCore.ParameterSet.Config as cms

def customTestInputFiles(process,era,runOnData,runWithAOD):
    if '2016' in era:
        if runOnData:
            process.source.fileNames = cms.untracked.vstring('')
        else:
            process.source.fileNames = cms.untracked.vstring('file:/eos/cms/store/cmst3/group/top/106X_mcRun2_asymptotic_v17-v1/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root')
