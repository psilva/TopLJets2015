import os

def getEraConfiguration(era,isData,preVFP=False):
    
    """ defines global tags, JEC/R corrections, etc. depending on the era """

    globalTags = {
        'era2016':('106X_mcRun2_asymptotic_v17',      '106X_dataRun2_v35'),
        }
    jecFiles    = {
        'era2016':['Summer19UL16_V7_MC', 'Summer19UL16_RunBCDEFGH_Combined_V7_DATA', 'Summer19UL16_V7_MC_UncertaintySources_AK4PFchs'],
        }
    jerFiles    = {
        'era2016':['Summer20UL16_JRV3_MC',   'Summer20UL16_JRV3_DATA'],
        }
    muonFiles   = {
        'era2016':'RoccoR2016bUL.txt',
        }
    globalTag = globalTags[era][isData]
    jecFile   = jecFiles[era][isData]
    jecTag    = '_'.join( jecFile.split('_')[0:-1] )
    jecDB     = 'jec_DATA.db'  if isData else 'jec_MC.db'
    jerFile   = jerFiles[era][isData]
    jerTag    = '_'.join( jerFile.split('_')[0:-1] )
    jerDB     = 'jer_DATA.db'  if isData else 'jer_MC.db'
    qgDBFile  = 'QGL_AK4chs_94X.db'
    muonDBFile = muonFiles[era]
    #ctppsDBFile= 'CTPPSRPAlignment_real_offline_v7.db'

    #customize based on dataset
    if preVFP:
        if not isData and '20UL16MiniAODAPV' in dataset:
            globalTag='102X_dataRun2_Prompt_v14'
            jecFiles['era2016'][0]='Summer19UL16APV_V7_MC'
            jecFiles['era2016'][2]='Summer19UL16APV_V7_MC_UncertaintySources_AK4PFchs'
            jerFiles['era2016']=['Summer20UL16APV_JRV3_MC','Summer20UL16_JRV3_DATA.db']
            muonDBFile = 'RoccoR2016aUL.txt'
            print '[Warn] Global tag has been overriden to',globalTag,'for',dataset,'in EraConfig.py'
            print '       muon scales changed to',muonDBFile

    #copy correction files to a common CMSSW search path directory
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s/%s.db %s'%(era,jecFile,jecDB))
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s/%s.db %s'%(era,jecFile,jecDB))
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s/%s.db %s'%(era,jerFile,jerDB))
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s/%s.txt jecUncSources.txt'%(era,jecFiles[era][2]))
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s qg_db.db'%(qgDBFile))
    #os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s ctpps_db.db'%(ctppsDBFile))
    os.system('cp ${CMSSW_BASE}/src/TopLJets2015/TopAnalysis/data/%s/%s muoncorr_db.txt'%(era,muonDBFile))

    print 'JEC tag: ',jecTag,'to be read from',jecDB
    print 'JER tag: ',jerTag,'to be read from',jerDB
    print 'Muon corrections to be read from muoncorr_db.txt'
    print 'q/g discriminator to be read from qg_db.db'
    #print 'CTPPS config to be read from ctpps_db.db'

    return globalTag, jecTag, jecDB, jerTag, jerDB
    
